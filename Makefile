all: pdf

pdf:
	pdflatex philosophy_of_science
	bibtex philosophy_of_science
	pdflatex philosophy_of_science
	pdflatex philosophy_of_science
	pdflatex philosophy_of_science

clean:
	rm -f *.aux *.log *.bbl *.blg *.toc *.lof *.lot *.out
